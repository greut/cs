module gitlab.com/greut/cs

go 1.13

replace (
	github.com/exoscale/egoscale => github.com/greut/egoscale v1.29.0
	gopkg.ini/ini.v1 => github.com/go-ini/ini v1.57.0
)

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/alecthomas/chroma v0.8.0
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/exoscale/egoscale v0.28.1
	github.com/go-ini/ini v1.57.0
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c // indirect
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	gopkg.in/ini.v1 v1.57.0 // indirect
)
