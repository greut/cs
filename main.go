package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/user"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/exoscale/egoscale"
	"github.com/go-ini/ini"
	cli "github.com/urfave/cli/v2"
)

var _client = new(egoscale.Client)
var _zone = regexp.MustCompile("^[a-z]{2}-[a-z]{2,3}-[1-9]$")

const (
	defaultEndpoint = "https://api.exoscale.com/v1"
	defaultZone     = "de-fra-1"
)

type options struct {
	debug       bool
	dryRun      bool
	dryJSON     bool
	accountName string
	region      string
	theme       string
	v2          bool
}

func main() {
	// global flags
	opts := new(options)
	var innerDebug bool
	var innerAccountName string
	var innerRegion string
	var innerDryRun bool

	// no prefix
	log.SetFlags(0)

	app := cli.NewApp()
	app.Name = "cs"
	app.HelpName = "cs"
	app.Usage = "CloudStack at the fingerprints"
	app.Description = "Exoscale Go CloudStack cli"
	app.Version = egoscale.Version
	app.Compiled = time.Now()
	app.EnableBashCompletion = true
	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:        "debug",
			Aliases:     []string{"d"},
			Usage:       "debug mode on",
			Destination: &opts.debug,
		},
		&cli.BoolFlag{
			Name:        "dry-run",
			Aliases:     []string{"D"},
			Usage:       "produce a cURL ready URL",
			Destination: &opts.dryRun,
			Hidden:      true,
		},
		&cli.BoolFlag{
			Name:        "dry-json",
			Aliases:     []string{"j"},
			Usage:       "produce a JSON preview of the query",
			Destination: &opts.dryJSON,
			Hidden:      true,
		},
		&cli.StringFlag{
			Name:        "region",
			Aliases:     []string{"r"},
			Usage:       "cloudstack.ini file section name",
			Destination: &opts.region,
		},
		&cli.StringFlag{
			Name:        "account",
			Aliases:     []string{"a"},
			Usage:       "exoscale.toml account name",
			Destination: &opts.accountName,
		},
		&cli.StringFlag{
			Name:        "theme",
			Aliases:     []string{"t"},
			Usage:       "syntax highlighting theme, see: https://xyproto.github.io/splash/docs/",
			Value:       "",
			Destination: &opts.theme,
		},
	}

	log.SetFlags(0)

	var method egoscale.Command
	app.Commands = buildCommands(&method, methods)

	for i, cmd := range app.Commands {
		// global, hidden debug flag
		cmd.Flags = append(cmd.Flags, &cli.BoolFlag{
			Name:        "debug",
			Aliases:     []string{"d"},
			Destination: &innerDebug,
			Hidden:      true,
		})
		cmd.Flags = append(cmd.Flags, &cli.BoolFlag{
			Name:        "dry-run",
			Aliases:     []string{"D"},
			Destination: &innerDryRun,
			Hidden:      true,
		})
		// global, hidden region flag
		cmd.Flags = append(cmd.Flags, &cli.StringFlag{
			Name:        "region",
			Aliases:     []string{"r"},
			Destination: &innerRegion,
			Hidden:      true,
		})
		// global, hidden account flag
		cmd.Flags = append(cmd.Flags, &cli.StringFlag{
			Name:        "account",
			Aliases:     []string{"a"},
			Destination: &innerAccountName,
			Hidden:      true,
		})
		app.Commands[i].Flags = cmd.Flags
	}

	// XXX we cap the args to 10 elements.
	var methodV2 string
	argsV2 := make([]reflect.Value, 10)

	commandV2 := &cli.Command{
		Name: "v2",
		Aliases: []string{
			"at-vie-1",
			"bg-sof-1",
			"ch-dk-2",
			"ch-gva-2",
			"de-fra-1",
			"de-muc-1",
		},
		Subcommands: buildCommandsV2(&methodV2, &argsV2, methodsV2),
	}

	app.Commands = append(app.Commands, commandV2)

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}

	if method == nil && methodV2 == "" {
		os.Exit(0)
	}

	// Picking a region
	if opts.region == "" {
		if innerRegion == "" {
			r, ok := os.LookupEnv("CLOUDSTACK_REGION")
			if ok {
				opts.region = r
			}
		} else {
			opts.region = innerRegion
		}
	}

	// Picking an account
	if opts.accountName == "" {
		if innerAccountName == "" {
			a, ok := os.LookupEnv("EXOSCALE_ACCOUNT")
			if ok {
				opts.accountName = a
			}
		} else {
			opts.accountName = innerAccountName
		}
	}

	opts.debug = opts.debug || innerDebug
	opts.dryRun = opts.dryRun || innerDryRun

	zone := ""
	for _, arg := range os.Args {
		if _zone.MatchString(arg) || zone == "v2" {
			zone = arg
			break
		}
	}

	client, _ := buildAccount(opts.accountName, zone)

	if client == nil {
		client, _ = buildClient(opts.region)
	}

	if opts.theme != "" {
		client.Theme = opts.theme
	}

	if method != nil {
		do(client, method, opts)
	} else {
		if err := doV2(client, methodV2, argsV2, opts); err != nil {
			log.Fatal(err)
		}
	}
}

func doV2(client *Client, method string, args []reflect.Value, opts *options) error {
	fn := reflect.ValueOf(client.V2).MethodByName(fmt.Sprintf("%sWithResponse", method))
	if fn.IsNil() {
		return fmt.Errorf("%s is not implemented", method)
	}

	args[0] = reflect.ValueOf(context.TODO())
	last := 0
	for _, arg := range args {
		if !arg.IsValid() {
			break
		}
		last++
	}

	values := fn.Call(args[:last])

	if len(values) != 2 {
		return fmt.Errorf("expected two values, got %d", len(values))
	}

	errValue := values[1]
	if !errValue.IsNil() {
		return errValue.Interface().(error)
	}

	statusCode := values[0].MethodByName("StatusCode").Call([]reflect.Value{})[0].Int()
	if statusCode > 200 {
		status := values[0].MethodByName("Status").Call([]reflect.Value{})[0].String()
		body := values[0].Elem().FieldByName("Body").Interface()
		return fmt.Errorf("%s failed with %s\n\n%s", method, status, string(body.([]byte)))
	}

	json200 := values[0].Elem().FieldByName("JSON200").Interface()
	out, _ := json.MarshalIndent(json200, "", "  ")
	printJSON(string(out), client.Theme)
	return nil
}

func do(client *Client, method egoscale.Command, opts *options) {
	// Show request and quit
	if opts.debug {
		payload, errP := client.Payload(method)
		if errP != nil {
			if _, err := fmt.Fprintf(os.Stderr, "cannot build payload: %s\n", errP); err != nil {
				log.Fatal(err)
			}
		} else {
			qs := payload.Encode()
			fmt.Printf("%s\\\n?%s\n", client.Endpoint, strings.Replace(qs, "&", "\\\n&", -1))
		}

		apiName := client.APIName(method)
		resp, err := client.Request(&egoscale.ListAPIs{Name: apiName})
		if err != nil {
			log.Fatal(err)
		}

		help, err := formatHelp(resp.(*egoscale.ListAPIsResponse).API[0])
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println()
		printJSON(string(help), client.Theme)

		os.Exit(0)
	}

	if opts.dryRun {
		payload, err := client.Payload(method)
		if err != nil {
			log.Fatal(err)
		}
		signature, err := client.Sign(payload)
		if err != nil {
			log.Fatal(err)
		}

		payload.Add("signature", signature)

		if _, err := fmt.Fprintf(os.Stdout, "%s?%s\n", client.Endpoint, payload.Encode()); err != nil {
			log.Fatal(err)
		}
		os.Exit(0)
	}

	if opts.dryJSON {
		request, err := json.MarshalIndent(method, "", "  ")
		if err != nil {
			log.Panic(err)
		}

		printJSON(string(request), client.Theme)
		os.Exit(0)
	}

	resp, err := client.Request(method)
	if err != nil {
		log.Fatal(err)
	}
	out, _ := json.MarshalIndent(&resp, "", "  ")
	printJSON(string(out), client.Theme)
}

func buildAccount(accountName, zone string) (*Client, error) {
	configHome := os.Getenv("XDG_CONFIG_HOME")
	if configHome == "" {
		usr, _ := user.Current()
		configHome = filepath.Join(usr.HomeDir, ".config")
	}

	localConfig := filepath.Join(configHome, "exoscale", "exoscale.toml")

	buf, err := ioutil.ReadFile(localConfig)
	if err != nil {
		return nil, err
	}

	var config Config
	if _, err := toml.Decode(string(buf), &config); err != nil {
		return nil, err
	}

	if accountName == "" {
		accountName = config.DefaultAccount
	}

	for _, acc := range config.Accounts {
		if acc.Name != accountName || acc.Account != accountName {
			continue
		}

		endpoint := defaultEndpoint
		if acc.Endpoint == "" {
			endpoint = acc.Endpoint
		}

		if zone == "v2" || zone == "" {
			if acc.DefaultZone != "" {
				zone = acc.DefaultZone
			} else {
				zone = defaultZone
			}
		}

		endpoint = strings.Replace(endpoint, "api.", fmt.Sprintf("api-%s.", zone), -1)

		if acc.Key == "" || acc.Secret == "" {
			log.Fatalf("Account %q is missing key or secret", accountName)
		}

		cs := egoscale.NewClient(endpoint, acc.Key, acc.Secret)

		client := &Client{
			Client:    cs,
			apiSecret: acc.Secret,
			Theme:     config.Theme,
		}

		return client, nil
	}

	return nil, fmt.Errorf("account %q was not found", accountName)
}

func buildClient(region string) (*Client, error) {
	usr, _ := user.Current()
	localConfig, _ := filepath.Abs("cloudstack.ini")
	inis := []string{
		localConfig,
		filepath.Join(usr.HomeDir, ".cloudstack.ini"),
	}
	config := ""
	for _, i := range inis {
		if _, err := os.Stat(i); err != nil {
			continue
		}
		config = i
		break
	}

	if config == "" {
		log.Fatalf("Config file not found within: %s", strings.Join(inis, ", "))
	}

	opts, err := ini.LoadSources(ini.LoadOptions{IgnoreInlineComment: true}, config)
	if err != nil {
		log.Fatal(err)
	}

	if region == "" {
		region = "cloudstack"
	}

	section, err := opts.GetSection(region)
	if err != nil {
		log.Fatalf("Section %q not found in the config file %s", region, config)
	}
	endpoint := defaultEndpoint
	ep, err := section.GetKey("endpoint")
	if err == nil {
		endpoint = ep.String()
	}

	key, errKey := section.GetKey("key")
	secret, errSecret := section.GetKey("secret")

	if errKey != nil || errSecret != nil {
		log.Fatalf("Section %q is missing key or secret", region)
	}

	cs := egoscale.NewClient(endpoint, key.String(), secret.String())

	client := &Client{cs, secret.String(), ""}

	th, err := section.GetKey("theme")
	if err == nil {
		client.Theme = th.String()
	} else {
		section, err = opts.GetSection("exoscale")
		if err == nil {
			theme, _ := section.GetKey("theme")
			client.Theme = theme.String()
		}
	}

	return client, nil
}

type genericV2 struct {
	argV2
	position int
	args     []reflect.Value
}

func (g *genericV2) Set(value string) error {
	g.args[g.position] = reflect.ValueOf(value)

	return nil
}

func (g *genericV2) String() string {
	value := g.args[g.position]

	if !value.IsValid() || value.IsNil() {
		return ""
	}

	return value.String()
}

func buildCommandsV2(out *string, args *[]reflect.Value, methods map[string][]cmdV2) []*cli.Command {
	commands := make([]*cli.Command, 0)

	for category, ms := range methods {
		for i := range ms {
			s := ms[i]

			name := s.method

			flags := make([]cli.Flag, len(s.args))
			for j, _ := range s.args {
				arg := s.args[j]

				flags[j] = &cli.GenericFlag{
					Name:     arg.name,
					Usage:    arg.usage,
					Required: true,
					Value: &genericV2{
						argV2:    arg,
						position: j + 1,
						args:     *args,
					},
				}
			}

			cmd := &cli.Command{
				Name:        name,
				Description: s.description,
				Category:    category,
				HideHelp:    s.hidden,
				Hidden:      s.hidden,
				Flags:       flags,
			}

			cmd.Action = func(c *cli.Context) error {
				if c.Args().Len() > 0 {
					return fmt.Errorf("unexpected extra arguments found: %s", strings.Join(c.Args().Slice(), " "))
				}

				*out = name
				return nil
			}

			commands = append(commands, cmd)
		}
	}

	return commands
}

func buildCommands(out *egoscale.Command, methods map[string][]cmd) []*cli.Command {
	commands := make([]*cli.Command, 0)

	for category, ms := range methods {
		for i := range ms {
			s := ms[i]

			name := _client.APIName(s.command)
			description := _client.APIDescription(s.command)

			cmd := &cli.Command{
				Name:        name,
				Description: description,
				Category:    category,
				HideHelp:    s.hidden,
				Hidden:      s.hidden,
				Flags:       buildFlags(s.command),
			}
			// report back the current command
			cmd.Action = func(c *cli.Context) error {
				if c.Args().Len() > 0 {
					return fmt.Errorf("unexpected extra arguments found: %s", strings.Join(c.Args().Slice(), " "))
				}
				*out = s.command
				return nil
			}
			// bash autocomplete
			cmd.BashComplete = func(c *cli.Context) {
				val := reflect.ValueOf(s.command)
				// we've got a pointer
				value := val.Elem()

				if value.Kind() != reflect.Struct {
					log.Fatalf("struct was expected")
				}

				ty := value.Type()
				for i := 0; i < value.NumField(); i++ {
					field := ty.Field(i)

					argName := ""
					if json, ok := field.Tag.Lookup("json"); ok {
						tags := strings.Split(json, ",")
						argName = tags[0]
					}

					if argName == "" {
						continue
					}

					if !c.IsSet(argName) {
						fmt.Printf("--%s\n", argName)
					}
				}
			}
			commands = append(commands, cmd)
		}
	}

	return commands
}

func buildFlags(method egoscale.Command) []cli.Flag {
	flags := make([]cli.Flag, 0)

	val := reflect.ValueOf(method)
	// we've got a pointer
	value := val.Elem()

	if value.Kind() != reflect.Struct {
		log.Fatalf("struct was expected")
		return flags
	}

	ty := value.Type()
	for i := 0; i < value.NumField(); i++ {
		field := ty.Field(i)

		if field.Name == "_" {
			continue
		}

		// XXX refactor with request.go
		var argName string
		required := false
		if json, ok := field.Tag.Lookup("json"); ok {
			tags := strings.Split(json, ",")
			argName = tags[0]
			required = true
			for _, tag := range tags {
				if tag == "omitempty" {
					required = false
				}
			}
			if argName == "" || argName == "omitempty" {
				continue
			}
		}

		description := ""
		if required {
			description = "required"
		}

		if doc, ok := field.Tag.Lookup("doc"); ok {
			if description != "" {
				description = fmt.Sprintf("[%s] %s", description, doc)
			} else {
				description = doc
			}
		}

		val := value.Field(i)
		addr := val.Addr().Interface()
		switch val.Kind() {
		case reflect.Bool:
			flags = append(flags, &cli.BoolFlag{
				Name:        argName,
				Usage:       description,
				Destination: addr.(*bool),
			})
		case reflect.Int:
			flags = append(flags, &cli.IntFlag{
				Name:        argName,
				Usage:       description,
				Destination: addr.(*int),
			})
		case reflect.Int64:
			flags = append(flags, &cli.Int64Flag{
				Name:        argName,
				Usage:       description,
				Destination: addr.(*int64),
			})
		case reflect.Uint:
			flags = append(flags, &cli.UintFlag{
				Name:        argName,
				Usage:       description,
				Destination: addr.(*uint),
			})
		case reflect.Uint64:
			flags = append(flags, &cli.Uint64Flag{
				Name:        argName,
				Usage:       description,
				Destination: addr.(*uint64),
			})
		case reflect.Float64:
			flags = append(flags, &cli.Float64Flag{
				Name:        argName,
				Usage:       description,
				Destination: addr.(*float64),
			})
		case reflect.Int16:
			flag := &cli.GenericFlag{
				Name:  argName,
				Usage: description,
			}
			typeName := field.Type.Name()
			if typeName != "int16" {
				flag.Value = &intTypeGeneric{
					addr:    addr,
					base:    10,
					bitSize: 16,
					typ:     field.Type,
				}
			} else {
				flag.Value = &int16Generic{
					value: addr.(*int16),
				}
			}
			flags = append(flags, flag)
		case reflect.Uint8:
			flags = append(flags, &cli.GenericFlag{
				Name:  argName,
				Usage: description,
				Value: &uint8Generic{
					value: addr.(*uint8),
				},
			})
		case reflect.Uint16:
			flags = append(flags, &cli.GenericFlag{
				Name:  argName,
				Usage: description,
				Value: &uint16Generic{
					value: addr.(*uint16),
				},
			})
		case reflect.String:
			typeName := field.Type.Name()
			if typeName != "string" {
				flags = append(flags, &cli.GenericFlag{
					Name:  argName,
					Usage: description,
					Value: &stringerTypeGeneric{
						addr: addr,
						typ:  field.Type,
					},
				})
			} else {
				flags = append(flags, &cli.StringFlag{
					Name:        argName,
					Usage:       description,
					Destination: addr.(*string),
				})
			}
		case reflect.Slice:
			switch field.Type.Elem().Kind() {
			case reflect.Uint8:
				ip := addr.(*net.IP)
				if *ip == nil || (*ip).Equal(net.IPv4zero) || (*ip).Equal(net.IPv6zero) {
					flags = append(flags, &cli.GenericFlag{
						Name:  argName,
						Usage: description,
						Value: &ipGeneric{
							value: ip,
						},
					})
				}
			case reflect.String:
				flags = append(flags, &cli.GenericFlag{
					Name:  argName,
					Usage: description,
					Value: &stringListGeneric{
						value: addr.(*[]string),
					},
				})
			default:
				switch field.Type.Elem() {
				case reflect.TypeOf(egoscale.ResourceTag{}):
					flags = append(flags, &cli.GenericFlag{
						Name:  argName,
						Usage: description,
						Value: &tagGeneric{
							value: addr.(*[]egoscale.ResourceTag),
						},
					})
				case reflect.TypeOf(egoscale.CIDR{}):
					flags = append(flags, &cli.GenericFlag{
						Name:  argName,
						Usage: description,
						Value: &cidrListGeneric{
							value: addr.(*[]egoscale.CIDR),
						},
					})
				case reflect.TypeOf(egoscale.UUID{}):
					flags = append(flags, &cli.GenericFlag{
						Name:  argName,
						Usage: description,
						Value: &uuidListGeneric{
							value: addr.(*[]egoscale.UUID),
						},
					})
				case reflect.TypeOf(egoscale.UserSecurityGroup{}):
					flags = append(flags, &cli.GenericFlag{
						Name:  argName,
						Usage: description,
						Value: &userSecurityGroupListGeneric{
							value: addr.(*[]egoscale.UserSecurityGroup),
						},
					})
				default:
					//log.Printf("[SKIP] Slice of %s is not supported!", field.Name)
				}
			}
		case reflect.Map:
			key := reflect.TypeOf(val.Interface()).Key()
			switch key.Kind() {
			case reflect.String:
				flags = append(flags, &cli.GenericFlag{
					Name:  argName,
					Usage: description,
					Value: &mapGeneric{
						value: addr.(*map[string]string),
					},
				})
			default:
				log.Printf("[SKIP] Type map for %s is not supported!", field.Name)
			}
		case reflect.Ptr:
			switch field.Type.Elem() {
			case reflect.TypeOf(true):
				flags = append(flags, &cli.GenericFlag{
					Name: argName,
					Value: &boolPtrGeneric{
						value: addr.(**bool),
					},
				})
			case reflect.TypeOf(egoscale.CIDR{}):
				flags = append(flags, &cli.GenericFlag{
					Name:  argName,
					Usage: description,
					Value: &cidrGeneric{
						value: addr.(**egoscale.CIDR),
					},
				})
			case reflect.TypeOf(egoscale.UUID{}):
				flags = append(flags, &cli.GenericFlag{
					Name:  argName,
					Usage: description,
					Value: &uuidGeneric{
						value: addr.(**egoscale.UUID),
					},
				})
			default:
				log.Printf("[SKIP] Ptr type of %s is not supported!", field.Name)
			}
		default:
			log.Printf("[SKIP] Type of %s is not supported! %v", field.Name, val.Kind())
		}
	}

	return flags
}

// Client holds the internal meta information for the cli
type Client struct {
	*egoscale.Client
	apiSecret string
	Theme     string
}

func formatHelp(api egoscale.API) ([]byte, error) {
	response := make(map[string]interface{})
	gather(response, api.Response)

	return json.MarshalIndent(response, "", "  ")
}

func gather(r map[string]interface{}, fields []egoscale.APIField) {
	for _, field := range fields {
		if len(field.Response) > 0 {
			response := make(map[string]interface{})
			gather(response, field.Response)
			r[field.Name] = []map[string]interface{}{response}
		} else {
			r[field.Name] = fmt.Sprintf("%s (%s) - %s", field.Name, field.Type, field.Description)
		}
	}
}
