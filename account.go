package main

// Config represents the main exoscale.toml file structure
type Config struct {
	DefaultAccount string
	Accounts       []Account
	Theme          string
}

// Account represents a block of configuration within the exoscale.toml file.
type Account struct {
	Name        string
	DefaultZone string
	Account     string
	Endpoint    string
	Key         string
	Secret      string
}
