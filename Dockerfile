FROM alpine:3

# hadolint ignore=DL3018
RUN apk --update --no-cache add \
        ca-certificates

COPY docker-entrypoint.sh /usr/local/bin/
COPY cs /usr/local/bin/

USER nobody

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["cs"]
