package main

type cmdV2 struct {
	method      string
	description string
	args        []argV2
	hidden      bool
}

type argV2 struct {
	name     string
	usage    string
	position int
}

var methodsV2 = map[string][]cmdV2{
	"zones": {
		{
			method: "ListZones",
		},
	},
	"network load balancer": {
		{
			method:      "ListLoadBalancers",
			description: "TODO",
		},
		{
			method:      "GetLoadBalancer",
			description: "TODO",
			args: []argV2{
				{
					name:  "id",
					usage: "load-balancer identifier",
				},
			},
		},
		{
			method:      "DeleteLoadBalancer",
			description: "TODO",
			args: []argV2{
				{
					name:  "id",
					usage: "load-balancer identifier",
				},
			},
		},
		{
			method:      "GetLoadBalancerService",
			description: "TODO",
			args: []argV2{
				{
					name:  "id",
					usage: "load-balancer identifier",
				},
				{
					name:  "serviceId",
					usage: "service identifier",
				},
			},
		},
		{
			method:      "DeleteLoadBalancerService",
			description: "TODO",
			args: []argV2{
				{
					name:  "id",
					usage: "load-balancer identifier",
				},
				{
					name:  "serviceId",
					usage: "service identifier",
				},
			},
		},
	},
	"types": {
		{
			method:      "ListInstanceTypes",
			description: "TODO",
		},
		{
			method:      "GetInstanceType",
			description: "TODO",
			args: []argV2{
				{
					name:  "id",
					usage: "instance identifier",
				},
			},
		},
	},
}
