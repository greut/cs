# cs: Command-Line Interface for (Exoscale's) CloudStack

Like the Pythonic [cs](https://pypi.python.org/pypi/cs) but in Go, built upon [egoscale](https://github.com/exoscale/egoscale)

## Configuration

Create a config file `cloudstack.ini` or `$HOME/.cloudstack.ini`.

```ini
; Default region
[cloudstack]

; Exoscale credential
endpoint = https://api.exoscale.ch/compute
key = EXO...
secret = ...

theme = fruity


; Another region
[cloudstack:production]

endpoint = https://api.exoscale.ch/compute
key = EXO...
secret = ...

theme = vim


; global config for themes
[exoscale]
; Pygments theme, see: https://xyproto.github.io/splash/docs/
; dark
theme = monokai
; light
theme = tango
; no colors (only boldness is allowed)
theme = nocolors
```

**Exoscale** configuration managed by [exo cli](https://github.com/exoscale/cli) is also supported.

### Themes

Thanks to [Alec Thomas](http://swapoff.org/)' [chroma](https://github.com/alecthomas/chroma), it supports many themes for your output:

- <https://xyproto.github.io/splash/docs/>
- <https://help.farbox.com/pygments.html>

## `v2`

Exoscale V2 API is a work in progress but this tool provides a way to query it.

```
$ cs v2 --help
```

## Usage

Some of the flags around a command.

```
$ cs (-h | --help)              global help
$ cs <command> (-h | --help)    help of a specific command
$ cs <command> (-d | --debug)   show the command and its expected output
$ cs <command> (-D | --dry-run) show the signed command
$ cs (-r | --region) <region>   specify a different region, default `cloudstack`
```
