package main

import (
	"github.com/exoscale/egoscale"
)

type cmd struct {
	command egoscale.Command
	hidden  bool
}

var methods = map[string][]cmd{
	"network": {
		{&egoscale.CreateNetwork{}, false},
		{&egoscale.DeleteNetwork{}, false},
		{&egoscale.ListNetworks{}, false},
		{&egoscale.RestartNetwork{}, true},
		{&egoscale.UpdateNetwork{}, false},
	},
	"virtual machine": {
		{&egoscale.AddNicToVirtualMachine{}, false},
		{&egoscale.ChangeServiceForVirtualMachine{}, false},
		{&egoscale.DeployVirtualMachine{}, false},
		{&egoscale.DestroyVirtualMachine{}, false},
		{&egoscale.ExpungeVirtualMachine{}, false},
		{&egoscale.GetVMPassword{}, false},
		{&egoscale.GetVirtualMachineUserData{}, false},
		{&egoscale.ListVirtualMachines{}, false},
		{&egoscale.RebootVirtualMachine{}, false},
		{&egoscale.RecoverVirtualMachine{}, false},
		{&egoscale.RemoveNicFromVirtualMachine{}, false},
		{&egoscale.ResetPasswordForVirtualMachine{}, false},
		{&egoscale.RestoreVirtualMachine{}, false},
		{&egoscale.ScaleVirtualMachine{}, false},
		{&egoscale.StartVirtualMachine{}, false},
		{&egoscale.StopVirtualMachine{}, false},
		{&egoscale.UpdateDefaultNicForVirtualMachine{}, true},
		{&egoscale.UpdateVirtualMachine{}, false},
		{&egoscale.UpdateVMNicIP{}, false},
	},
	"iso": {
		{&egoscale.AttachISO{}, false},
		{&egoscale.DetachISO{}, false},
		{&egoscale.ListISOs{}, false},
	},
	"volume": {
		{&egoscale.ListVolumes{}, false},
		{&egoscale.ResizeVolume{}, false},
	},
	"template": {
		{&egoscale.DeleteTemplate{}, false},
		{&egoscale.ListOSCategories{}, true},
		{&egoscale.ListTemplates{}, false},
		{&egoscale.RegisterCustomTemplate{}, false},
	},
	"account": {
		{&egoscale.ListAccounts{}, false},
	},
	"zone": {
		{&egoscale.ListZones{}, false},
	},
	"snapshot": {
		{&egoscale.CreateSnapshot{}, false},
		{&egoscale.DeleteSnapshot{}, false},
		{&egoscale.ExportSnapshot{}, false},
		{&egoscale.ListSnapshots{}, false},
		{&egoscale.RevertSnapshot{}, false},
	},
	"user": {
		{&egoscale.ListUsers{}, false},
		{&egoscale.RegisterUserKeys{}, false},
	},
	"security group": {
		{&egoscale.AuthorizeSecurityGroupEgress{}, false},
		{&egoscale.AuthorizeSecurityGroupIngress{}, false},
		{&egoscale.CreateSecurityGroup{}, false},
		{&egoscale.DeleteSecurityGroup{}, false},
		{&egoscale.ListSecurityGroups{}, false},
		{&egoscale.RevokeSecurityGroupEgress{}, false},
		{&egoscale.RevokeSecurityGroupIngress{}, false},
	},
	"ssh": {
		{&egoscale.RegisterSSHKeyPair{}, false},
		{&egoscale.ListSSHKeyPairs{}, false},
		{&egoscale.CreateSSHKeyPair{}, false},
		{&egoscale.DeleteSSHKeyPair{}, false},
		{&egoscale.ResetSSHKeyForVirtualMachine{}, false},
	},
	"affinity group": {
		{&egoscale.CreateAffinityGroup{}, false},
		{&egoscale.DeleteAffinityGroup{}, false},
		{&egoscale.ListAffinityGroups{}, false},
		{&egoscale.UpdateVMAffinityGroup{}, false},
	},
	//"anti-affinity group": {
	//	{&egoscale.CreateAntiAffinityGroup{}, false},
	//	{&egoscale.DeleteAntiAffinityGroup{}, false},
	//	{&egoscale.ListAntiAffinityGroups{}, false},
	//},
	"vm group": {
		{&egoscale.CreateInstanceGroup{}, false},
		{&egoscale.DeleteInstanceGroup{}, false},
		{&egoscale.ListInstanceGroups{}, false},
		{&egoscale.UpdateInstanceGroup{}, false},
	},
	"tags": {
		{&egoscale.CreateTags{}, false},
		{&egoscale.DeleteTags{}, false},
		{&egoscale.ListTags{}, false},
	},
	"nic": {
		{&egoscale.ActivateIP6{}, false},
		{&egoscale.AddIPToNic{}, false},
		{&egoscale.ListNics{}, false},
		{&egoscale.RemoveIPFromNic{}, false},
	},
	"address": {
		{&egoscale.AssociateIPAddress{}, false},
		{&egoscale.DisassociateIPAddress{}, false},
		{&egoscale.ListPublicIPAddresses{}, false},
		{&egoscale.UpdateIPAddress{}, false},
	},
	"async job": {
		{&egoscale.QueryAsyncJobResult{}, false},
		{&egoscale.ListAsyncJobs{}, false},
	},
	"apis": {
		{&egoscale.ListAPIs{}, false},
	},
	"iam": {
		{&egoscale.CreateAPIKey{}, false},
		{&egoscale.ListAPIKeys{}, false},
		{&egoscale.ListAPIKeyOperations{}, false},
		{&egoscale.RevokeAPIKey{}, false},
	},
	"instance pool": {
		{&egoscale.CreateInstancePool{}, false},
		{&egoscale.DestroyInstancePool{}, false},
		{&egoscale.EvictInstancePoolMembers{}, false},
		{&egoscale.GetInstancePool{}, false},
		{&egoscale.ListInstancePools{}, false},
		{&egoscale.ScaleInstancePool{}, false},
		{&egoscale.UpdateInstancePool{}, false},
	},
	"event": {
		{&egoscale.ListEventTypes{}, false},
		{&egoscale.ListEvents{}, false},
	},
	"offerings": {
		{&egoscale.ListResourceDetails{}, false},
		{&egoscale.ListResourceLimits{}, false},
		{&egoscale.ListServiceOfferings{}, false},
	},
	"reversedns": {
		{&egoscale.DeleteReverseDNSFromPublicIPAddress{}, false},
		{&egoscale.QueryReverseDNSForPublicIPAddress{}, false},
		{&egoscale.UpdateReverseDNSForPublicIPAddress{}, false},
		{&egoscale.DeleteReverseDNSFromVirtualMachine{}, false},
		{&egoscale.QueryReverseDNSForVirtualMachine{}, false},
		{&egoscale.UpdateReverseDNSForVirtualMachine{}, false},
	},
	//"dns": {
	//	{&egoscale.CreateDNSDomainRecord{}, false},
	//	{&egoscale.CreateDNSDomain{}, false},
	//	{&egoscale.DeleteDNSDomainRecord{}, false},
	//	{&egoscale.DeleteDNSDomain{}, false},
	//	{&egoscale.GetDNSDomainRecord{}, false},
	//	{&egoscale.GetDNSDomainZoneFile{}, false},
	//	{&egoscale.ListDNSDomainRecords{}, false},
	//	{&egoscale.ListDNSDomains{}, false},
	//	{&egoscale.ResetDNSDomainToken{}, false},
	//	{&egoscale.UpdateDNSDomainRecord{}, false},
	//},
}
